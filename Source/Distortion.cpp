﻿/*
  ==============================================================================

    Distortion.cpp
    Created: 12 Nov 2018 2:38:47pm
    Author:  Mateusz

  ==============================================================================
*/

#include "Distortion.h"
#include <math.h>

float Distortion::processSample(float sample)
{
	const float in = sample;
	float rtn = 0.f;

	if (distortionType == Distortion::Type::HardClipping)
		rtn = processHardClipping(inputGain * in);
	else if (distortionType == Distortion::Type::SoftClipping)
		rtn = processSoftClipping(inputGain * in);
	else if (distortionType == Distortion::Type::SoftClippingExponential)
		rtn = processSoftClippingExponentional(inputGain * in);
	else if (distortionType == Distortion::Type::FullWaveRectifier)
		rtn = processFullWaveRectifier(inputGain * in);
	else if (distortionType == Distortion::Type::FullHalfRectifier)
		rtn = processHalfWaveRectifier(inputGain * in);
	else if (distortionType == Distortion::Type::DAEPI_WaveShaper)
		rtn = processWaveshaper(in);
	else if (distortionType == Distortion::Type::MY)
		rtn = processMy(in);

	// always limit output to <-1, 1>
	rtn = processHardClipping(rtn);

	return rtn;
}

float Distortion::processHardClipping(float in)
{
	float threshold = 1.f;
	if (in > threshold)
		return threshold;
	else if (in < -threshold)
		return -threshold;
	else
		return in;
}

float Distortion::processSoftClipping(float in)
{
	constexpr float threshold1 = 1.f / 3.f;
	constexpr float threshold2 = 2.f / 3.f;

	if (in > threshold2)
		return 1.f;
	else if (in > threshold1)
		return (3.f - (2.f - 3.f * in) * (2.f + 3.f * in)) / 3.f;
	else if (in < -threshold2)
		return -1.f;
	else if (in < -threshold1)
		return -(3.f - (2.f - 3.f * in) * (2.f + 3.f * in)) / 3.f;
	else
		return 2.f * in;
}

float Distortion::processSoftClippingExponentional(float in)
{
	if (in > 0)
		return 1.f - expf(-in);
	else
		return -1.f + expf(in);
}

float Distortion::processFullWaveRectifier(float in)
{
	return fabsf(in);
}

float Distortion::processHalfWaveRectifier(float in)
{
	return in > 0 ? in : 0.f;

}

float Distortion::processWaveshaper(float in)
{
	float out;

	if (in >= 0)
		out = (1.f / atan(inputGain)) * atan(inputGain * in);
	else
		out = (1.f / atan(inputGainNegative)) * atan(inputGainNegative * in);
	
	return -1.f * out;
}

float Distortion::processMy(float in)
{
	return in > 0 ? 1.f - in : in < 0 ? -1.f - in : 0.f;
}