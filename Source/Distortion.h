/*
  ==============================================================================

    Distortion.h
    Created: 12 Nov 2018 2:38:47pm
    Author:  Mateusz

  ==============================================================================
*/

#pragma once

class Distortion
{
public:
	enum Type
	{
		HardClipping,
		SoftClipping,
		SoftClippingExponential,
		FullWaveRectifier,
		FullHalfRectifier,
		DAEPI_WaveShaper,
		MY,
		numTypes
	};

	void setType(Type type) { distortionType = type;  }
	void setInputGain(float inGain) { inputGain = inGain;  }
	void setInputGainNegative(float inGainN) { inputGainNegative = inGainN; }

	float processSample(float sample);
private:
	Type distortionType = Type::HardClipping;
	float inputGain = 1.f;
	float inputGainNegative = 1.f;

	float processHardClipping(float in);
	float processSoftClipping(float in);
	float processSoftClippingExponentional(float in);
	float processFullWaveRectifier(float in);
	float processHalfWaveRectifier(float in);
	float processWaveshaper(float in);
	float processMy(float in);
};