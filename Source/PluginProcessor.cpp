/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin processor.

  ==============================================================================
*/

#include "PluginProcessor.h"

String percentValueToTextFunction(float value, int maxLen)
{
	auto percent = value * 100.f;
	return String(percent);
}

float percentTextToValueFunction(const String& text)
{
	auto f = text.getFloatValue();
	return f / 100.f;
}


AudioProcessorValueTreeState::ParameterLayout createParameterLayout()
{
	std::vector<std::unique_ptr<RangedAudioParameter>> params;

	params.push_back(std::make_unique<AudioParameterFloat>(
		"inputGain", "inputGain",
		NormalisableRange<float>(1.f, 100.f), 1.f,
		"%"//,
		//AudioProcessorParameter::genericParameter,
		//percentValueToTextFunction,
		//percentTextToValueFunction
		));

	params.push_back(std::make_unique<AudioParameterFloat>(
		"inputGainNegative", "inputGainNegative",
		NormalisableRange<float>(1.f, 100.f), 1.f,
		"%"//,
		   //AudioProcessorParameter::genericParameter,
		   //percentValueToTextFunction,
		   //percentTextToValueFunction
		));

	params.push_back(std::make_unique<AudioParameterChoice>("type", "type", types, 0));

	return { params.begin(), params.end() };
}

//==============================================================================
DistortionAudioProcessor::DistortionAudioProcessor()
	: state(*this, nullptr, "DistortionAudioProcessor", createParameterLayout())
#ifndef JucePlugin_PreferredChannelConfigurations
     , AudioProcessor (BusesProperties()
                     #if ! JucePlugin_IsMidiEffect
                      #if ! JucePlugin_IsSynth
                       .withInput  ("Input",  AudioChannelSet::stereo(), true)
                      #endif
                       .withOutput ("Output", AudioChannelSet::stereo(), true)
                     #endif
                       )
#endif
{
	inputGainControll = dynamic_cast<AudioParameterFloat*> (state.getParameter("inputGain"));
	inputGainControll->addListener(this);

	inputGainNegativeControll = dynamic_cast<AudioParameterFloat*> (state.getParameter("inputGainNegative"));
	inputGainNegativeControll->addListener(this);

	typeControll = dynamic_cast<AudioParameterChoice*> (state.getParameter("type"));
	typeControll->addListener(this);
}

DistortionAudioProcessor::~DistortionAudioProcessor()
{
}

//==============================================================================
const String DistortionAudioProcessor::getName() const
{
    return JucePlugin_Name;
}

bool DistortionAudioProcessor::acceptsMidi() const
{
   #if JucePlugin_WantsMidiInput
    return true;
   #else
    return false;
   #endif
}

bool DistortionAudioProcessor::producesMidi() const
{
   #if JucePlugin_ProducesMidiOutput
    return true;
   #else
    return false;
   #endif
}

bool DistortionAudioProcessor::isMidiEffect() const
{
   #if JucePlugin_IsMidiEffect
    return true;
   #else
    return false;
   #endif
}

double DistortionAudioProcessor::getTailLengthSeconds() const
{
    return 0.0;
}

int DistortionAudioProcessor::getNumPrograms()
{
    return 1;   // NB: some hosts don't cope very well if you tell them there are 0 programs,
                // so this should be at least 1, even if you're not really implementing programs.
}

int DistortionAudioProcessor::getCurrentProgram()
{
    return 0;
}

void DistortionAudioProcessor::setCurrentProgram (int index)
{
}

const String DistortionAudioProcessor::getProgramName (int index)
{
    return {};
}

void DistortionAudioProcessor::changeProgramName (int index, const String& newName)
{
}

//==============================================================================
void DistortionAudioProcessor::prepareToPlay (double sampleRate, int samplesPerBlock)
{
	//distortion.setInputGain(inputGainControll->get());
	//distortion.setInputGain(inputGainNegativeControll->get());
	//distortion.setType(static_cast<Distortion::Type>(typeControll->getIndex()));
}

void DistortionAudioProcessor::releaseResources()
{
    // When playback stops, you can use this as an opportunity to free up any
    // spare memory, etc.
}

#ifndef JucePlugin_PreferredChannelConfigurations
bool DistortionAudioProcessor::isBusesLayoutSupported (const BusesLayout& layouts) const
{
  #if JucePlugin_IsMidiEffect
    ignoreUnused (layouts);
    return true;
  #else
    // This is the place where you check if the layout is supported.
    // In this template code we only support mono or stereo.
    if (layouts.getMainOutputChannelSet() != AudioChannelSet::mono()
     && layouts.getMainOutputChannelSet() != AudioChannelSet::stereo())
        return false;

    // This checks if the input layout matches the output layout
   #if ! JucePlugin_IsSynth
    if (layouts.getMainOutputChannelSet() != layouts.getMainInputChannelSet())
        return false;
   #endif

    return true;
  #endif
}
#endif

void DistortionAudioProcessor::processBlock (AudioBuffer<float>& buffer, MidiBuffer& midiMessages)
{
    ScopedNoDenormals noDenormals;
    auto totalNumInputChannels  = getTotalNumInputChannels();
    auto totalNumOutputChannels = getTotalNumOutputChannels();

    // In case we have more outputs than inputs, this code clears any output
    // channels that didn't contain input data, (because these aren't
    // guaranteed to be empty - they may contain garbage).
    // This is here to avoid people getting screaming feedback
    // when they first compile a plugin, but obviously you don't need to keep
    // this code if your algorithm always overwrites all the output channels.
    for (auto i = totalNumInputChannels; i < totalNumOutputChannels; ++i)
        buffer.clear (i, 0, buffer.getNumSamples());

    // This is the place where you'd normally do the guts of your plugin's
    // audio processing...
    // Make sure to reset the state if your inner loop is processing
    // the samples and the outer loop is handling the channels.
    // Alternatively, you can process the samples with the channels
    // interleaved by keeping the same state.
    for (int channel = 0; channel < totalNumInputChannels; ++channel)
    {
        auto* channelData = buffer.getWritePointer (channel);

		for (int smpIdx = 0; smpIdx < buffer.getNumSamples(); ++smpIdx, ++channelData)
			*channelData = distortion.processSample(*channelData);		
    }
}

//==============================================================================
bool DistortionAudioProcessor::hasEditor() const
{
    return true; // (change this to false if you choose to not supply an editor)
}

AudioProcessorEditor* DistortionAudioProcessor::createEditor()
{
	return new GenericAudioProcessorEditor(this);
}

//==============================================================================
void DistortionAudioProcessor::getStateInformation (MemoryBlock& destData)
{
	auto s = state.copyState();
	std::unique_ptr<XmlElement> xml(s.createXml());
	copyXmlToBinary(*xml, destData);
}

void DistortionAudioProcessor::setStateInformation (const void* data, int sizeInBytes)
{
	std::unique_ptr<XmlElement> xmlState(getXmlFromBinary(data, sizeInBytes));
	if (xmlState.get() != nullptr)
		if (xmlState->hasTagName(state.state.getType()))
			state.replaceState(ValueTree::fromXml(*xmlState));
}

//==============================================================================
// This creates new instances of the plugin..
AudioProcessor* JUCE_CALLTYPE createPluginFilter()
{
    return new DistortionAudioProcessor();
}


void DistortionAudioProcessor::parameterValueChanged(int parameterIndex, float newValue)
{
	if (parameterIndex == inputGainControll->getParameterIndex())
		distortion.setInputGain(inputGainControll->get());
	else if (parameterIndex == inputGainNegativeControll->getParameterIndex())
		distortion.setInputGainNegative(inputGainNegativeControll->get());
	else if (parameterIndex == typeControll->getParameterIndex())
		distortion.setType(static_cast<Distortion::Type>(typeControll->getIndex()));
}

void DistortionAudioProcessor::parameterGestureChanged(int, bool)
{}